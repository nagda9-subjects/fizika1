%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculates the inductance per unit length of a coaxial cable
%
% Author: Dr. Zsolt Szab�
% P�zm�ny P�ter Catholic University
%
% The input data are exported from the PDE Modeler:
% export PDE coefficients; you will need c, which contains the magnetic permeabilities of the calculation domain 
% export the mesh (p - points, t - triangles) 
% export the potential u (vector potential A)
%
%
% These softwares are "as-are" and carries no warranty.  
% They can be used under GNU licence without restriction for research and education purposes. 
% Bug reports: Szab� Zsolt, szabo.zsolt@itk.ppke.hu
%
% Updated 10 April, 2020.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
mu_0 = 4*pi*1e-7; %magnetic permeability of free space

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%input data required for the derived formulas only
r1    = 1e-2;   %(m) the radius of the inner cylindrical core
r2    = 2.2e-2; %(m) the inner side of the cilidrical enclosure
r3    = 2.5e-2; %(m) the outer side of the cilidrical enclosure

I = 2; %A
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%the current densities
%the inner core
Jb = I/(pi*r1*r1)
%the exterior cylindrical shell
Jk = -I/(pi*(r3*r3-r2*r2))

%Calculate the inductance from the data of PDE Modeler
%calculate the components of the magnetic induction B = curl(A)
[dAdx,dAdy] = pdegrad(p,t,u);
Bxt = dAdy;
Byt = -dAdx;

%here it is multiplied with mu_0 because I have set mu_0 = 1 in PDE
%Modeler
Bx = pdeprtni(p,t,Bxt)*mu_0;
By = pdeprtni(p,t,Byt)*mu_0;

%the distnce between 0 and rc is divided in Nx elements
Nx = 1000;
x = linspace(0,r3,Nx);

%calculate the components of B required for the calculation of the magnetic flux (the other component is not
%required due to scalar product with the surface normal)
By_d = tri2grid(p,t,By,x,0);

%integrate with the trapezoidal method
fluxB_d = trapz(x,By_d);

%the self inductance of the coaxial cable
L = fluxB_d/I

%The closed formula 
L_frm = mu_0/(2*pi)*(log(r2/r1) + r3*r3/(r3*r3 - r2*r2)*log(r3/r2))

%the components of the magnetic field at R = 1.5e-2 and phi = pi/3
%the coordinates of the field point
R   = 1.5e-2;
phi = pi/3;

x_f = R*cos(phi);
y_f = R*sin(phi);

%interpolate Bx_f and By_f
Bx_f = tri2grid(p,t,Bx,x_f,y_f)
By_f = tri2grid(p,t,By,x_f,y_f)

%a m�gneses indukci� a kerestt pontban
B_phi = mu_0*I/(2*pi)/R;
Bx_frm = -B_phi*sin(phi)
By_frm =  B_phi*cos(phi)
