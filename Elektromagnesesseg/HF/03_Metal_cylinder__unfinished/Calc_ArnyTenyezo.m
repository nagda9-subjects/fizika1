%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculates the magnetic field inside and outside of a magnetic cylinder 
% placed in homogeneous magnetic field and the screening factor 
%
% Author: Dr. Zsolt Szab�
% P�zm�ny P�ter Catholic University
%
% The input data are exported from the PDE Modeler:
% export the mesh (p - points, t - triangles) 
% export the potential u
%
% ra the outer radius of the cylinder
% rb the inner radius of the cylinder
% mu_r the magetic permeability
%
% These softwares are "as-are" and carries no warranty.  
% They can be used under GNU licence without restriction for research and education purposes. 
% Bug reports: Szab� Zsolt, szabo.zsolt@itk.ppke.hu
%
% Updated 1 March, 2020.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
mu_0 = 4*pi*1e-7; %magnetic permeability of free space

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%input data required for the derived formulas only
ra    = 25e-2; %(m) the radius of the inner cylinder
rb    = 20e-2; %(m) the radius of the outer cylinder

mu_r = 1000;   %the electric permittivity between the cylinders
H0   = 200; %(A/m) the homogeneous source field
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%calculate the components of the magnetic induction B = curl(A)
[dAdx,dAdy] = pdegrad(p,t,u);
Bxt = dAdy;
Byt = -dAdx;

Bx = pdeprtni(p,t,Bxt);
By = pdeprtni(p,t,Byt);

%the components of the magnetic field at R = 0 and phi = 0
%the coordinates of the field point
R   = 0;
phi = 0;
%R   = [0 5e-2 10e-2 15e-2]; %some random points inside of the shell, so the screening factor should be the same
%phi = [0 pi/3 -pi/6 3*pi/4];
x_f = R.*cos(phi);
y_f = R.*sin(phi);

%interpolate Bx_f and By_f
Bx_f = tri2grid(p,t,Bx,x_f,y_f);
By_f = tri2grid(p,t,By,x_f,y_f);

Hx_f = Bx_f/(mu_0);
Hy_f = By_f/(mu_0);

%the screening factor
G = sqrt(Hx_f.*Hx_f + Hy_f.*Hy_f)/H0

%the exact expression
c_B1 = 4*mu_r*ra*ra/(ra*ra*(mu_r + 1)*(mu_r + 1) - rb*rb*(mu_r - 1)*(mu_r - 1));

G_an = c_B1

error = abs(G - G_an)/G_an*100