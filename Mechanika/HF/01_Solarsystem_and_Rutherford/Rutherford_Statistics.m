clear all;
close all;
clc;

qe = 1.620217662e-19;       % egy elektron t�lt�se
epsnull = 8.8541878128e-12; % v�kuum dielektromos �lland�ja
k = 1/(4*pi*epsnull);       % Coulomb-f�le ar�nyoss�gi t�nyez�

q_alpha = 2*qe;             % alpha r�szecske t�lt�se
m_alpha = 6.644657230e-27;  % alpha r�szecske t�mege
q_gold = 79*qe;             % arany t�lt�se
R_gold = 1.74e-10;          % arany atom sugara
R_proton = 8.775e-12;       % proton sugara
R_nucleous = 0.7e-14;   % arany atommag sugara

% Forr�sok:
% https://hu.wikipedia.org/wiki/Proton
% https://periodictable.com/Elements/079/data.wt.html

n1 = 100;           % szimul�lt r�szecsk�k sz�ma
dist1 = 0.1e-18;    % r�szecsk�k ry(1) koordin�t�i k�zti t�vols�g, els� r�szecske kezdeti t�vols�ga az y = 0-t�l

figure;
subplot(4,3,1)
hold on;
gold_core(197, R_nucleous, R_proton);
[frx1, fry1] = Runge_Kutta(n1, dist1, R_nucleous, m_alpha, k, q_alpha, q_gold);
for j = 1:n1
    plot(frx1, fry1(j,:), 'b');
end
xlabel('$r_x(t)$', 'Interpreter', 'latex');
ylabel('$r_y(t)$', 'Interpreter', 'latex');
xlim([-5*R_nucleous 5*R_nucleous]);
ylim([-5*R_nucleous 5*R_nucleous]);
title('ry(1) dist = 0.1e-18', 'Interpreter', 'latex');

n2 = 100;
dist2 = 0.1e-17;

subplot(4,3,2)
hold on;
gold_core(197, R_nucleous, R_proton);
[frx2, fry2] = Runge_Kutta(n2, dist2, R_nucleous, m_alpha, k, q_alpha, q_gold);
for j = 1:n2
    plot(frx2, fry2(j,:), 'b');
end
xlabel('$r_x(t)$', 'Interpreter', 'latex');
ylabel('$r_y(t)$', 'Interpreter', 'latex');
xlim([-5*R_nucleous 5*R_nucleous]);
ylim([-5*R_nucleous 5*R_nucleous]);
title('ry(1) dist = 0.1e-17', 'Interpreter', 'latex');

n3 = 1000;
dist3 = 0.1e-13;

subplot(4,3,3)
hold on;
gold_core(197, R_nucleous, R_proton);
[frx3, fry3] = Runge_Kutta(n3, dist3, R_nucleous, m_alpha, k, q_alpha, q_gold);
for j = 1:n3
    plot(frx3, fry3(j,:), 'b');
end
xlabel('$r_x(t)$', 'Interpreter', 'latex');
ylabel('$r_y(t)$', 'Interpreter', 'latex');
xlim([-5*R_nucleous 5*R_nucleous]);
ylim([-5*R_nucleous 5*R_nucleous]);
title('ry(1) dist = 0.1e-13', 'Interpreter', 'latex');

subplot(4,3,[4,6])
hold on;
gold_core(197, R_nucleous, R_proton);
plot(frx3, fry3(n3/2+1,:), 'b');
line_x = linspace(-5*R_nucleous,5*R_nucleous);
line_y = line_x.*0+fry3(n3/2+1, 1);
plot(line_x, line_y, 'b--');
xlabel('$r_x(t)$', 'Interpreter', 'latex');
ylabel('$r_y(t)$', 'Interpreter', 'latex');
xlim([-5*R_nucleous 5*R_nucleous]);
ylim([-5*R_nucleous 5*R_nucleous]);
title('Slope of the path', 'Interpreter', 'latex');

% Egyenes illeszt�s a poz�ci�kra a meredek meg�llap�t�s�hoz
warning('off','all')
P1 = polyfit(frx3(end-int16(length(frx3)/2):end), fry3(n3/2+1,end-int16(length(frx3)/2):end),1);
P1_val = polyval(P1,frx3);
plot(frx3, P1_val);

subplot(4,3,[7,9])
theta = [];
for j= 1:n3
    P2 = polyfit(frx3(end-int16(length(frx3)/2):end), fry3(j,end-int16(length(frx3)/2):end),1);
    theta(end+1) = atand(P2(1));
end
scatter(fry3(:, 1),theta);
xlabel('$r_y(t=0) $', 'Interpreter', 'latex');
ylabel('$\theta$ in degrees', 'Interpreter', 'latex');
title('Angle of deflection', 'Interpreter', 'latex');

subplot(4,3,[10, 12]);
beta = round(theta, 1, 'significant');
[freq, num] = groupcounts(beta(:));
plot(num, freq);
xlabel('\theta angle deflection $', 'Interpreter', 'latex');
ylabel('frequency', 'Interpreter', 'latex');
title('Atomic number = 79', 'Interpreter', 'latex');

% Arany atom t�megsz�m�nak m�dos�t�sa
q_gold = 50*qe;

n4 = 100;
dist4 = 0.1e-13;

figure;
subplot(3,1,1)
hold on;
gold_core(100, R_nucleous, R_proton);
[frx4, fry4] = Runge_Kutta(n4, dist4, R_nucleous, m_alpha, k, q_alpha, q_gold);
for j = 1:n4
    plot(frx4, fry4(j,:), 'b');
end
xlabel('$r_x(t)$', 'Interpreter', 'latex');
ylabel('$r_y(t)$', 'Interpreter', 'latex');
xlim([-5*R_nucleous 5*R_nucleous]);
ylim([-5*R_nucleous 5*R_nucleous]);
title('Atomic number = 50, ry(1) dist = 0.1e-13', 'Interpreter', 'latex');

% Egyenes illeszt�s a poz�ci�kra a meredek meg�llap�t�s�hoz
warning('off','all')
P3 = polyfit(frx4(end-int16(length(frx4)/2):end), fry4(n4/2+1,end-int16(length(frx4)/2):end),1);
P3_val = polyval(P3,frx4);
plot(frx4, P3_val);

subplot(3,1,2)
theta2 = [];
for j= 1:n4
    P4 = polyfit(frx4(end-int16(length(frx4)/2):end), fry4(j,end-int16(length(frx4)/2):end),1);
    theta2(end+1) = atand(P4(1));
end
scatter(fry4(:, 1),theta2);
xlabel('$r_y(t=0) $', 'Interpreter', 'latex');
ylabel('$\theta$ in degrees', 'Interpreter', 'latex');
title('Atomic number = 50', 'Interpreter', 'latex');

subplot(4,3,[10, 12]);
beta2 = round(theta2, 1, 'significant');
[freq2, num2] = groupcounts(beta2(:));
plot(num2, freq2);
xlabel('\theta angle deflection $', 'Interpreter', 'latex');
ylabel('frequency', 'Interpreter', 'latex');
title('Atomic number = 50', 'Interpreter', 'latex');

function gold_core(n, D_radius, C_radius)
    theta = rand(n,1)*(2*pi);
    r = sqrt(rand(n,1))*D_radius;
    x = r.*cos(theta);
    y = r.*sin(theta);
    for i = 1:n
        circle(x(i), y(i), C_radius/5e3);
    end
end

function h = circle(x,y,r)
    th = 0:pi/100:2*pi;
    xunit = r * cos(th) + x;
    yunit = r * sin(th) + y;
    hold on;
    h = plot(xunit, yunit, 'y-');
end

function [frx, fry] = Runge_Kutta (n, dist, R_nucleous, m_alpha, k, q_alpha, q_gold)
    fvx(1) = 1.53*10e+7;
    fvy(1) = 0;
    frx(1) = -100*R_nucleous;

    dt = 0.25*R_nucleous/fvx(1);
    t = 0:dt:dt*1e3;

    for j = 1:n
        fry(j,1) = -n/2*dist + j*dist;

        for i = 2:length(t);
            fvxk1 = (1/m_alpha)*k*(q_alpha*q_gold)*frx(i-1)*dt/(frx(i-1)^2+fry(j,i-1)^2)^(3/2);
            fvyk1 = (1/m_alpha)*k*(q_alpha*q_gold)*fry(j,i-1)*dt/(frx(i-1)^2+fry(j,i-1)^2)^(3/2);
            frxk1 = dt*fvx(i-1);
            fryk1 = dt*fvy(i-1);

            fvxk2 = (1/m_alpha)*k*(q_alpha*q_gold)*(frx(i-1)+frxk1/2)*dt/((frx(i-1)+frxk1/2)^2+(fry(j,i-1)+fryk1/2)^2)^(3/2);
            fvyk2 = (1/m_alpha)*k*(q_alpha*q_gold)*(fry(j,i-1)+fryk1/2)*dt/((frx(i-1)+frxk1/2)^2+(fry(j,i-1)+fryk1/2)^2)^(3/2);
            frxk2 = dt*(fvx(i-1)+fvxk1/2);
            fryk2 = dt*(fvy(i-1)+fvyk1/2);

            fvxk3 = (1/m_alpha)*k*(q_alpha*q_gold)*(frx(i-1)+frxk2/2)*dt/((frx(i-1)+frxk2/2)^2+(fry(j,i-1)+fryk2/2)^2)^(3/2);
            fvyk3 = (1/m_alpha)*k*(q_alpha*q_gold)*(fry(j,i-1)+fryk2/2)*dt/((frx(i-1)+frxk2/2)^2+(fry(j,i-1)+fryk2/2)^2)^(3/2);
            frxk3 = dt*(fvx(i-1)+fvxk2/2);
            fryk3 = dt*(fvy(i-1)+fvyk2/2);

            fvxk4 = (1/m_alpha)*k*(q_alpha*q_gold)*(frx(i-1)+frxk3)*dt/((frx(i-1)+frxk3)^2+(fry(j,i-1)+fryk3)^2)^(3/2);
            fvyk4 = (1/m_alpha)*k*(q_alpha*q_gold)*(fry(j,i-1)+fryk3)*dt/((frx(i-1)+frxk3)^2+(fry(j,i-1)+fryk3)^2)^(3/2);
            frxk4 = dt*(fvx(i-1)+fvxk3);
            fryk4 = dt*(fvy(i-1)+fvyk3);

            fvx(i) = fvx(i-1)+(fvxk1+2*fvxk2+2*fvxk3+fvxk4)/6;
            fvy(i) = fvy(i-1)+(fvyk1+2*fvyk2+2*fvyk3+fvyk4)/6;
            frx(i) = frx(i-1)+(frxk1+2*frxk2+2*frxk3+frxk4)/6;
            fry(j, i) = fry(j,i-1)+(fryk1+2*fryk2+2*fryk3+fryk4)/6;
        end
    end
end