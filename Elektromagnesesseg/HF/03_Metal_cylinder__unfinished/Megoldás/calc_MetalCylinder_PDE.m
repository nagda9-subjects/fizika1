%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculates the potential and the electric field of a PEC cylinder
%
% Author: Dr. Zsolt Szab�
% P�zm�ny P�ter Catholic University
%
% The input data are exported from the PDE Modeler:
% export PDE coefficients; you will need c, which contains the electric permittivities epsilon_r of the calculation domain 
% export the mesh (p - points, t - triangles) 
% export the potential u
%
%
% These softwares are "as-are" and carries no warranty.  
% They can be used under GNU licence without restriction for research and education purposes. 
% Bug reports: Szab� Zsolt, szabo.zsolt@itk.ppke.hu
%
% Updated 22 April, 2020.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
eps_0 = 8.854187e-12; %electric permittivity of free space

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%input data required for the derived formulas only
a     = 5e-2; %(m) the radius of the PEC cylinder
eps_r = 1; %the media around the cylinder
E0    = 1000; %(V/m) the voltage between the cylinders
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%the coordinates of the field point
R   = 5.5e-2;
phi = 0;
x_f = R*cos(phi);
y_f = R*sin(phi);

%compare the potential
u_f   = tri2grid(p,t,u,x_f,y_f)
%the exact expression
u_frm = -E0*(R - a*a/(R))*cos(phi)

%calculate the components of the electric induction D
[mDxt, mDyt] = pdecgrad(p,t,c,u); %this function evaluates -D as constant in each triangle of the FE mesh

% calculate Dx and Dy on the nodes of the triangular FE mesh
Dx = -pdeprtni(p,t,mDxt);
Dy = -pdeprtni(p,t,mDyt);

%the components of the electric field at R and phi
%interpolate Dx_f and Dy_f
Dx_f = tri2grid(p,t,Dx,x_f,y_f);
Dy_f = tri2grid(p,t,Dy,x_f,y_f);

Ex_f = Dx_f/(eps_0)
Ey_f = Dy_f/(eps_0)

%the exact expression
Ex = E0*(1 + a*a/(R*R))
Ey = 0

