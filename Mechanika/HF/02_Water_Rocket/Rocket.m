clear all;
close all;
clc;

%% Constants
V_all = 0.001;       % 1L in m^3
mb = 0.2;            % Mass of the bottle rocket
An0 = 0.0001;        % Area of nozzle
Pa = 101.325;       % atmospheric pressure of Earth in Pascal

%% Initial conditions
V0 = 0.5*V_all;      % Volume of compressed air (0.5 L) when it is filled halfway with water
P0 = 344737.865;     % Pressure of compressed air in pascal, 50 psi in Pascal
v0 = 0;
r0 = 0;

%%
dt = 0.001;
t = 0:dt:5;

[V, P, m, v, r] = calculation(mb, V_all, V0, P0, An0, r0, v0, dt, t);

figure;
subplot(2,1,1);
plot(t,r);
xlabel('$t$', 'Interpreter', 'latex');
ylabel('$r(t)$', 'Interpreter', 'latex');
title("Position vs Time", 'Interpreter', 'latex');

subplot(2,1,2);
plot(t,v);
xlabel('$t$', 'Interpreter', 'latex');
ylabel('$v(t)$', 'Interpreter', 'latex');
title("Speed vs Time", 'Interpreter', 'latex');


%%
V_span = linspace(0, V_all, 20);            % initial air volumes
P_span = linspace(Pa, 551580.583, 20);      % initial inner pressures (between atmospheric pressure and 80 psi)
[V_inits, P_inits] = meshgrid(V_all-V_span, P_span); % mivel a v�z �s nem a leveg� f�ggv�ny�ben akarunk �br�zolni

% figure;
for i = V_span
    for j = P_span
        [V, P, m, v, r] = calculation(mb, V_all, i, j, An0, r0, v0, dt, t);
        
        V_idx = (V_span == i);
        P_idx = (P_span == j);
        R_max(P_idx, V_idx) = max(r);   % mivel az R_max m�trixban V szerinti lesz a sor index, �s P szerinti az oszlop index, viszont a griden a V szerinti az X koordin�ta, �s P szerinti az y koordin�ta, ez�rt fel kell cser�lni az indexeket
        
%         [r_max, t_idx] = max(r); 
%         plot(t,r, 'b-');
%         hold on;
%         plot(t(t_idx), r_max, 'ro');
%         xlabel('$t$', 'Interpreter', 'latex');
%         ylabel('$r(t)$', 'Interpreter', 'latex');
%         title("Position vs Time", 'Interpreter', 'latex');
%         drawnow;
%         pause(0.1);
    end
end

figure;
surf(V_inits, P_inits, R_max);
colorbar;
title('Maxim�lis magass�g kezdeti nyom�s �s t�rfogat f�ggv�ny�ben');
xlabel('V_{0} kezdeti v�z t�rfogat (m^{3})');
ylabel('P_{0} kezdeti nyom�s (Pa)');
zlabel('maxim�lis magass�g (m)');

[max_at_P, idx_V] = max(R_max');
V_inits_at_max = V_span(idx_V);

table(P_span', V_inits_at_max','VariableNames', {'Initial Pressure (Pa)', 'Initial Volume(m^3)'})

%%
An_span = linspace(0, 0.001, 100);

for i = An_span
    [V, P, m, v, r] = calculation(mb, V_all, V0, P0, i, r0, v0, dt, t);
    [r_max, t_idx] = max(r);
    An_idx = (An_span == i);
    
    R_max2(An_idx) = max(r);
end

[M, idx_An] = max(R_max2);
ideal_nosle_size = An_span(idx_An)

figure;
plot(An_span, R_max2);
title('Maxim�lis magass�g a kupak m�ret f�ggv�ny�ben');
xlabel('An_{0} kezdeti ter�let (m^{2})');
ylabel('Magass�g (m)');


%%
function [V, P, m, v, r] = calculation(mb, V_all, V0, P0, An0, r0, v0, dt, t)
    % set constants
    g = 9.81;           % gravitational constant
    Ro_water = 997;     % density of water 
    k = 1.401;          % Specific Heat Ratio of air at temperature 30�C
    c = An0*100;            % coefficient of air drag
    Pa = 101.325;       % atmospheric pressure of Earth in Pascal
    
    % References
    % https://en.wikipedia.org/wiki/Density_of_air
    % https://en.wikipedia.org/wiki/Water
    % https://en.wikipedia.org/wiki/Atmospheric_pressure
    
    % set initial conditions
    An = An0;           % Area of nozzle
    V(1) = V0;          % Volume of compressed air (0.5 L) when it is filled halfway with water
    P(1) = P0;          % Pressure of compressed air in pascal, 50 psi in Pascal
    m(1) = mb + Ro_water*(V_all-V(1));
    v(1) = v0;
    r(1) = r0;

    for i = 1:length(t)-1
        if V(i) >= V_all    % mivel a s�r�tett leveg� t�rfogata csak addig konstans, am�g van v�z, ut�na v�gtelen
            V(i+1) = V_all;
            P(i+1) = Pa;
        else
            V(i+1) = V(i)+dt*An*((2*(P(i)-Pa))/Ro_water)^(1/2);
            P(i+1) = max(Pa, P(i)*(V(i)/V(i+1))^k);
        end
        m(i+1) = mb + Ro_water*(max(0, V_all-V(i+1)));
        v(i+1) = v(i) + dt*1/m(i)*(2*An*(P(i)-Pa)-m(i)*g-v(i)*c);
        r(i+1) = max(0,r(i) + v(i)*dt);
    end
end

function rocket(x0, y0)
    scale = 1;

    % colors
    lightBlue = [91, 207, 244]/255;
    silver = [192, 192, 192]/255;
    slateGrey = [112,128,144]/255;
    grey = [128,128,128]/255;
    
    txt = 'APOLLO-II';
    
    square(:,1) = x0 + scale*[1 1 -1 -1 1];
    square(:,2) = y0 + scale*[0 7 7 0 0];
    
    rect(:,1,1) = x0 + scale*[-1 -1 -2 -1];
    rect(:,2,1) = y0 + scale*[3 0 0 3];
    rect(:,1,2) = x0 + scale*[1 1 2 1];
    rect(:,2,2) = y0 + scale*[3 0 0 3];
    
    traps(:,1) = x0 + scale*[-0.7 -1.1 1.1 0.7];
    traps(:,2) = y0 + scale*[0 -0.5 -0.5 0];
    
    for i=1:3
        c(:,: ,i) = circle(x0, y0+scale*(1.5*i+1), scale*0.5);
    end
    
    p_x = -scale:0.001:scale;
    p_y = -(1/scale)*2*p_x.^2 + y0+scale*9;
    p = [p_x.',p_y.'];
    
    hold on;
    axis equal;
    fill(square(:,1), square(:,2), slateGrey);
    fill(p(:,1), p(:,2), silver);
    fill(traps(:,1), traps(:,2), 'b');
    text(x0, y0+scale*1, txt, 'HorizontalAlignment','center');
    for i = 1:3
        fill(c(:,1,i), c(:,2,i),'b');
    end 
    for i = 1:2
        fill(rect(:,1,i), rect(:,2,i), 'b');
    end
    xlim([-4, 4]);
    ylim([y0-0.5, y0+10]);
end

function s = circle(x,y,r)
    th = 0:pi/20:2*pi;
    c_x = r * cos(th) + x;
    c_y = r * sin(th) + y;
    s = [c_x.', c_y.'];
end