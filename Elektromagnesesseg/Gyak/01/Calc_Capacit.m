%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate the capacitance per unit length of two concentric cylinders
% by applying the Gauss law over a rectangular closed surface
%
% Author: Dr. Zsolt Szab�
% P�zm�ny P�ter Catholic University
%
% The input data are exported from the PDE Modeler:
% export PDE coefficients; you will need c, which contains the electric permittivities epsilon_r of the calculation domain 
% export the mesh (p - points, t - triangles) 
% export the potential u
%
% (x1,y1) and (x2,y2) are the coordinates of the rectangular closed surface
%
% These softwares are "as-are" and carries no warranty.  
% They can be used under GNU licence without restriction for research and education purposes. 
% Bug reports: Szab� Zsolt, szabo.zsolt@itk.ppke.hu
%
% Updated 20 February, 2020.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
eps_0 = 8.854187e-12; %electric permittivity of free space

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%input data required for the derived formulas only
ra    = 1e-2; %(m) the radius of the inner cylinder
rb    = 2e-2; %(m) the radius of the outer cylinder

eps_r = 4;   %the electric permittivity between the cylinders
U     = 2.5; %(V) the voltage between the cylinders
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%calculate the components of the electric induction D 
[mDxt, mDyt] = pdecgrad(p,t,c,u); %this function evaluates -D as constant in each triangle of the FE mesh

% calculate Dx and Dy on the nodes of the triangular FE mesh
Dx = -pdeprtni(p,t,mDxt);
Dy = -pdeprtni(p,t,mDyt);

%the rectangular closed surface is divided in Nx and Ny elements
Nx = 1000;
Ny = 1000;

%the lower coordinates of the rectangle
x1 = -1.2e-2; 
y1 = -1.2e-2;
%the upper coordinates of the rectangle
x2 = 1.2e-2; 
y2 = 1.2e-2;

x = linspace(x1,x2,Nx);
y = linspace(y1,y2,Nx);

%calculate the components of D required for the calculation of the flux of D on the
%sides of the rectangular closed surface (the other components are not
%required due to scalar product with the surface normal)
%down
Dy_d = tri2grid(p,t,Dy,x,y1);
%up
Dy_u = tri2grid(p,t,Dy,x,y2);
%left
Dx_l = tri2grid(p,t,Dx,x1,y);
%rigth
Dx_r = tri2grid(p,t,Dx,x2,y);

%integrate with the trapezoidal method
fluxD_d = -trapz(x,Dy_d);
fluxD_u =  trapz(x,Dy_u);
fluxD_l = -trapz(y,Dx_l);
fluxD_r =  trapz(y,Dx_r);

%the free charge over unit length of the inner cylinder
q_free   = eps_0*(fluxD_d + fluxD_u + fluxD_l + fluxD_r)

%the exact expression
q_free_frm = 2*pi*eps_0*eps_r*U/log(rb/ra)

%the components of the electric field at R = 1.5e-2 and phi = 0
%the coordinates of the field point
R   = 1.5e-2;
phi = 0;
x_f = R*cos(phi);
y_f = R*sin(phi);

%interpolate Dx_f and Dy_f
Dx_f = tri2grid(p,t,Dx,x_f,y_f);
Dy_f = tri2grid(p,t,Dy,x_f,y_f);

Ex_f = Dx_f/(eps_r)
Ey_f = Dy_f/(eps_r)

%the exact expression
Ex = U/(R*log(rb/ra))
Ey = 0

%the capacitance per unit length
C     = q_free/U;

C_frm = 2*pi*eps_0*eps_r/log(rb/ra)