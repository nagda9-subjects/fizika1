%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculates the current density at distance s from the inner cylinder of a coaxial cable 
%
% Author: Dr. Zsolt Szab�
% P�zm�ny P�ter Catholic University
%
% The input data are exported from the PDE Modeler:
% export the mesh (p - points, t - triangles) 
% export the potential u
%
%
% These softwares are "as-are" and carries no warranty.  
% They can be used under GNU licence without restriction for research and education purposes. 
% Bug reports: Szab� Zsolt, szabo.zsolt@itk.ppke.hu
%
% Updated 1 March, 2020.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
eps_0 = 8.854187e-12; %electric permittivity of free space

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%input data
ra    = 1e-2; %(m) the radius of the inner cylinder
rb    = 2e-2; %(m) the radius of the outer cylinder

sigma = 5;
U     = 10; %(V) the voltage between the cylinders
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%!!!!!!!!!!!!!!!!!!!
%there is a bug or convenience ???? in PDE toolbox because the exported u is multiplied by sigma
%!!!!!!!!!!!!!!!!!!!

%calculate the components of the electric induction D 
[mDxt, mDyt] = pdecgrad(p,t,c,u); %this function evaluates -D as constant in each triangle of the FE mesh

% calculate Dx and Dy on the nodes of the triangular FE mesh
Dx = -pdeprtni(p,t,mDxt);
Dy = -pdeprtni(p,t,mDyt);

%the rectangular closed surface is divided in Nx and Ny elements
Nx = 1000;
Ny = 1000;

%the lower coordinates of the rectangle
x1 = -1.3e-2; 
y1 = -1.3e-2;
%the upper coordinates of the rectangle
x2 = 1.3e-2; 
y2 = 1.3e-2;

x = linspace(x1,x2,Nx);
y = linspace(y1,y2,Ny);
%calculate the components of D required for the calculation of the flux of D on the
%sides of the rectangular closed surface (the other components are not
%required due to scalar product with the surface normal)
%down
Dy_d = tri2grid(p,t,Dy,x,y1);
%up
Dy_u = tri2grid(p,t,Dy,x,y2);
%left
Dx_l = tri2grid(p,t,Dx,x1,y);
%rigth
Dx_r = tri2grid(p,t,Dx,x2,y);

%integrate with the trapezoidal method
fluxD_d = -trapz(x,Dy_d);
fluxD_u =  trapz(x,Dy_u);
fluxD_l = -trapz(y,Dx_l);
fluxD_r =  trapz(y,Dx_r);

%the current per unit length between the cylinders  
I = fluxD_d + fluxD_u + fluxD_l + fluxD_r

%the exact expression
I_frm = 2*pi*sigma*U/log(rb/ra)

