%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculates the magnetic field at distance s from a straight wire carrying steady current 
%
% Author: Dr. Zsolt Szab�
% P�zm�ny P�ter Catholic University
%
% The input data are exported from the PDE Modeler:
% export the mesh (p - points, t - triangles) 
% export the potential u
%
% s the distance from the straigth wire
% I the steady current 
%
% These softwares are "as-are" and carries no warranty.  
% They can be used under GNU licence without restriction for research and education purposes. 
% Bug reports: Szab� Zsolt, szabo.zsolt@itk.ppke.hu
%
% Updated 1 March, 2020.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
mu_0 = 4*pi*1e-7; %magnetic permeability of free space

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%input data required for the derived formulas only
s = 0.05;  % (m) the distance from the wire where the field is calculated
I = 0.3; %(A)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%calculate the components of the magnetic induction B = curl(A)
[dAdx,dAdy] = pdegrad(p,t,u);
Bxt = dAdy;
Byt = -dAdx;

Bx = pdeprtni(p,t,Bxt);
By = pdeprtni(p,t,Byt);

%the components of the magnetic field at s and phi = 0
%the coordinates of the field point
phi = 0;
x_f = s.*cos(phi);
y_f = s.*sin(phi);

%interpolate Bx_f and By_f
Bx_f = tri2grid(p,t,Bx,x_f,y_f)
By_f = tri2grid(p,t,By,x_f,y_f)

Hx_f = Bx_f/(mu_0)
Hy_f = By_f/(mu_0)
