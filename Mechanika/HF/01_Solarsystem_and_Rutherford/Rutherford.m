clear all;
close all;
clc;

qe = 1.620217662e-19;   % egy elektron t�lt�se
epsnull = 8.8541878128e-12; % v�kuum dielektromos �lland�ja
k = 1/(4*pi*epsnull);   % Coulomb-f�le ar�nyoss�gi t�nyez�

q_alpha = 2*qe;         % alpha r�szecske t�lt�se
m_alpha = 6.644657230*10e-27; % alpha r�szecske t�mege
q_gold = 79*qe;         % arany t�lt�se
R_gold = 1.74e-10;      % arany atom sugara 
R_covalent = 1.36e-10;  % arany atomok k�zti t�vols�g kovalens k�t�s sor�n

v0 = 1.53e+8;           % alpha r�szecske kezdeti sebess�ge

% Forr�sok:
% https://periodictable.com/Elements/079/data.wt.html
% https://hu.wikipedia.org/wiki/Coulomb-t�rv�ny
% https://en.wikipedia.org/wiki/Alpha_particle


%%
% Euler

dt = 10e-25;
t = 0:dt:dt*2e+6;

rx(1) = -R_gold;
ry(1) = 0.2e-16;
vx(1) = v0;
vy(1) = 0;

for i = 2:length(t);
    vx(i) = (1/m_alpha)*k*((q_alpha*q_gold)/(rx(i-1)^2+ry(i-1)^2)^(3/2))*rx(i-1)*dt + vx(i-1);
    vy(i) = (1/m_alpha)*k*((q_alpha*q_gold)/(rx(i-1)^2+ry(i-1)^2)^(3/2))*ry(i-1)*dt + vy(i-1);

    rx(i) = rx(i-1)+dt*vx(i-1);
    ry(i) = ry(i-1)+dt*vy(i-1);
end

%%
% Heun

dt = 10e-25;
t = 0:dt:dt*2e+6;

hrx(1) = -R_gold;
hry(1) = 0.2e-16;
hvx(1) = v0;
hvy(1) = 0;

for i = 2:length(t);
    hvx(i) = (1/m_alpha)*k*(q_alpha*q_gold)/2*(2*hrx(i-1)+dt*hvx(i-1))/(hrx(i-1)^2+hry(i-1)^2)^(3/2)*dt + hvx(i-1);
    hvy(i) = (1/m_alpha)*k*(q_alpha*q_gold)/2*(2*hry(i-1)+dt*hvy(i-1))/(hrx(i-1)^2+hry(i-1)^2)^(3/2)*dt + hvy(i-1);

    hrx(i) = dt/2*(2*hvx(i-1)+dt*(1/m_alpha)*k*(q_alpha*q_gold)*hrx(i-1)/(hrx(i-1)^2+hry(i-1)^2)^(3/2)) + hrx(i-1);
    hry(i) = dt/2*(2*hvy(i-1)+dt*(1/m_alpha)*k*(q_alpha*q_gold)*hry(i-1)/(hrx(i-1)^2+hry(i-1)^2)^(3/2)) + hry(i-1);
end

%%
% 2. Runge-Kutta

dt = 10e-25;
t = 0:dt:dt*2e+6;

srx(1) = -R_gold;
sry(1) = 0.2e-16;
svx(1) = v0;
svy(1) = 0;

for i = 2:length(t);
    svxk1 = (1/m_alpha)*k*(q_alpha*q_gold)*srx(i-1)*dt/(srx(i-1)^2+sry(i-1)^2)^(3/2);
    svyk1 = (1/m_alpha)*k*(q_alpha*q_gold)*sry(i-1)*dt/(srx(i-1)^2+sry(i-1)^2)^(3/2);
    srxk1 = dt*svx(i-1);
    sryk1 = dt*svy(i-1);

    svxk2 = (1/m_alpha)*k*(q_alpha*q_gold)*(srx(i-1)+srxk1/2)*dt/((srx(i-1)+srxk1/2)^2+(sry(i-1)+sryk1/2)^2)^(3/2);
    svyk2 = (1/m_alpha)*k*(q_alpha*q_gold)*(sry(i-1)+sryk1/2)*dt/((srx(i-1)+srxk1/2)^2+(sry(i-1)+sryk1/2)^2)^(3/2);
    srxk2 = dt*(svx(i-1)+svxk1/2);
    sryk2 = dt*(svy(i-1)+svyk1/2);

    svx(i) = svx(i-1)+svxk2;
    svy(i) = svy(i-1)+svyk2;
    srx(i) = srx(i-1)+srxk2;
    sry(i) = sry(i-1)+sryk2;
end

%%
% 4. Runge-Kutta

dt = 10e-25;
t = 0:dt:dt*2e+6;

frx(1) = -R_gold;
fry(1) = 0.2e-16;
fvx(1) = v0;
fvy(1) = 0;

for i = 2:length(t);
    fvxk1 = (1/m_alpha)*k*(q_alpha*q_gold)*frx(i-1)*dt/(frx(i-1)^2+fry(i-1)^2)^(3/2);
    fvyk1 = (1/m_alpha)*k*(q_alpha*q_gold)*fry(i-1)*dt/(frx(i-1)^2+fry(i-1)^2)^(3/2);
    frxk1 = dt*fvx(i-1);
    fryk1 = dt*fvy(i-1);

    fvxk2 = (1/m_alpha)*k*(q_alpha*q_gold)*(frx(i-1)+frxk1/2)*dt/((frx(i-1)+frxk1/2)^2+(fry(i-1)+fryk1/2)^2)^(3/2);
    fvyk2 = (1/m_alpha)*k*(q_alpha*q_gold)*(fry(i-1)+fryk1/2)*dt/((frx(i-1)+frxk1/2)^2+(fry(i-1)+fryk1/2)^2)^(3/2);
    frxk2 = dt*(fvx(i-1)+fvxk1/2);
    fryk2 = dt*(fvy(i-1)+fvyk1/2);

    fvxk3 = (1/m_alpha)*k*(q_alpha*q_gold)*(frx(i-1)+frxk2/2)*dt/((frx(i-1)+frxk2/2)^2+(fry(i-1)+fryk2/2)^2)^(3/2);
    fvyk3 = (1/m_alpha)*k*(q_alpha*q_gold)*(fry(i-1)+fryk2/2)*dt/((frx(i-1)+frxk2/2)^2+(fry(i-1)+fryk2/2)^2)^(3/2);
    frxk3 = dt*(fvx(i-1)+fvxk2/2);
    fryk3 = dt*(fvy(i-1)+fvyk2/2);

    fvxk4 = (1/m_alpha)*k*(q_alpha*q_gold)*(frx(i-1)+frxk3)*dt/((frx(i-1)+frxk3)^2+(fry(i-1)+fryk3)^2)^(3/2);
    fvyk4 = (1/m_alpha)*k*(q_alpha*q_gold)*(fry(i-1)+fryk3)*dt/((frx(i-1)+frxk3)^2+(fry(i-1)+fryk3)^2)^(3/2);
    frxk4 = dt*(fvx(i-1)+fvxk3);
    fryk4 = dt*(fvy(i-1)+fvyk3);

    fvx(i) = fvx(i-1)+(fvxk1+2*fvxk2+2*fvxk3+fvxk4)/6;
    fvy(i) = fvy(i-1)+(fvyk1+2*fvyk2+2*fvyk3+fvyk4)/6;
    frx(i) = frx(i-1)+(frxk1+2*frxk2+2*frxk3+frxk4)/6;
    fry(i) = fry(i-1)+(fryk1+2*fryk2+2*fryk3+fryk4)/6;
end

%%
% ODE45

orx(1) = -5*R_gold;
ory(1) = 0.2e-16;
ovx(1) = v0;
ovy(1) = 0;

f = @(t, y) [
            (1/m_alpha)*k*(q_alpha*q_gold)*y(3)/(y(3)^2+y(4)^2)^(3/2);
            (1/m_alpha)*k*(q_alpha*q_gold)*y(4)/(y(3)^2+y(4)^2)^(3/2);
            y(1);
            y(2);
            ];

[t, y] = ode45(f, [0 10e-25*2e+7], [ovx; ovy; orx; ory]);

%%
% 

figure;
subplot(3,2,1);
circle(0, 0, R_gold);
plot(0, 0, 'ro', 'MarkerFaceColor', 'r');
plot(rx, ry, 'b')
xlabel('$r_x(t)$', 'Interpreter', 'latex');
ylabel('$r_y(t)$', 'Interpreter', 'latex');
xlim([-R_gold-R_gold/2 R_gold+R_gold/2]);
ylim([-R_gold-R_gold/2 R_gold+R_gold/2]);
title("Eulers's method", 'Interpreter', 'latex');

subplot(3,2,2);
circle(0, 0, R_gold);
plot(0, 0, 'ro', 'MarkerFaceColor', 'r');
plot(hrx, hry, 'b')
xlabel('$r_x(t)$', 'Interpreter', 'latex');
ylabel('$r_y(t)$', 'Interpreter', 'latex');
xlim([-R_gold-R_gold/2 R_gold+R_gold/2]);
ylim([-R_gold-R_gold/2 R_gold+R_gold/2]);
title("Heun's method", 'Interpreter', 'latex');

subplot(3,2,3);
circle(0, 0, R_gold);
plot(0, 0, 'ro', 'MarkerFaceColor', 'r');
plot(srx, sry, 'b')
xlabel('$r_x(t)$', 'Interpreter', 'latex');
ylabel('$r_y(t)$', 'Interpreter', 'latex');
xlim([-R_gold-R_gold/2 R_gold+R_gold/2]);
ylim([-R_gold-R_gold/2 R_gold+R_gold/2]);
title('2nd order Runge-Kutta', 'Interpreter', 'latex');

subplot(3,2,4);
circle(0, 0, R_gold);
plot(0, 0, 'ro', 'MarkerFaceColor', 'r');
plot(frx, fry, 'b')
xlabel('$r_x(t)$', 'Interpreter', 'latex');
ylabel('$r_y(t)$', 'Interpreter', 'latex');
xlim([-R_gold-R_gold/2 R_gold+R_gold/2]);
ylim([-R_gold-R_gold/2 R_gold+R_gold/2]);
title('4th order Runge-Kutta', 'Interpreter', 'latex');

subplot(3,2,[5,6])
foil = [[], []];
for i = 1:5
    for j = 1:3
        h_dist = -2*3.5*R_covalent + i*2*R_covalent;
        H = (3-j)*R_covalent + h_dist;
        W = -2*R_covalent+(j-1)*2*R_covalent;
        foil(end+1, 1) = W;
        foil(end, 2) = H;
        circle(W, H, R_gold);
    end
end
plot(foil(:,1), foil(:,2), 'ro', 'MarkerFaceColor', 'r');
plot(y(:, 3), y(:, 4), 'b');
xlabel('$r_x(t)$', 'Interpreter', 'latex');
ylabel('$r_y(t)$', 'Interpreter', 'latex');
xlim([-6*R_gold 6*R_gold]);
ylim([-5*R_gold 5*R_gold]);
title('ODE45', 'Interpreter', 'latex');

%%
function h = circle(x,y,r)
th = 0:pi/100:2*pi;
xunit = r * cos(th) + x;
yunit = r * sin(th) + y;
h = plot(xunit, yunit, 'r-');
hold on;
end
