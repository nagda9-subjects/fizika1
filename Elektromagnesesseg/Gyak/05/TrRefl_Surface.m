%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculates the reflection and transmission of a plane waves with perpendicular incidence on a metallic surface 
%
% Author: Dr. Zsolt Szab�
% P�zm�ny P�ter Catholic University
%
% The input data are:
% n1 is the refractive index of medium 1, which is a lossless dielectris
% n2 is the refractive index of medium 2, which is a lossy metal (complex
% number)
% la is the free space wavelength of the incident wave
% E0 is the magnitude of the incident plane wave
% n_theta to plot the reflection coefficient in the interval 0:90 for
% parallel and perpendicular polarization
%
% The output data are
% 1. the reflectance in function of the incident angle for parallel and perpendicular polarization
% 2. the electric field at perpendicular incidence
%
% These softwares are "as-are" and carries no warranty.  
% They can be used under GNU licence without restriction for research and education purposes. 
% Bug reports: Szab� Zsolt, szabo.zsolt@itk.ppke.hu
%
% Updated 30 April, 2020.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%input data
%the refractive index of medium 1
n1 = 1; %air

%the refractive index of medium 2
n2 = 0.771 - sqrt(-1)*5.91; %Al at 495.8 nm

%the free space wavelength
la = 495.8; %nm
%the magnitude of the incident plane wave
E0 = 100;

%number of angles to calculate the Reflectance
n_theta = 100; 

%to visualize the reflection and transmission at perpendicular incidence
n_t   = 120;   %number of time steps

n_z   = 1000;  %number of space steps in front of the interface
n_z1  = 100;   %number of space steps behind the interface
z_min = -5*la; %the distance in front of the interface
z_max = la;    %the distance behind the interface

%to save the images in .bmp
elment = 1;  %0 do not write it out, 1 write it out
szam   = 1000; %the index of the first image
%!!!!!!!!!!!!
% change the folder name where you save the images in line 143
%!!!!!!!!!!!!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

theta_1 = linspace(0,pi/2,n_theta);

sin_theta_1 = sin(theta_1);
cos_theta_1 = sqrt(1 - sin_theta_1.*sin_theta_1);

%generalized Descartes law
sin_theta_2 = n1*sin(theta_1)/n2;
cos_theta_2 = sqrt(1 - sin_theta_2.*sin_theta_2);

%reflectance for parallel polarization
r_p = (n1*cos_theta_2 - n2*cos_theta_1)./(n1*cos_theta_2 + n2*cos_theta_1);
%reflectance for perpendcular polarization
r_m = (n1*cos_theta_1 - n2*cos_theta_2)./(n1*cos_theta_1 + n2*cos_theta_2);

%for normal incidence
r_p_0 = (n1 - n2)./(n1 + n2);
t_p_0 = 2*n1./(n1 + n2);

%the speed of light in vacuum
c0    = 299.792458; %fs/nm
frec  = c0/la;      %the frequency in PHz
om    = 2*pi*frec;  %angular frequency
kz1   = 2*pi*n1/la; %the wavenumber in medium 1
gamma = sqrt(-1)*2*pi*n2/la; %the wavenumber in medium 2
alpha = real(gamma);  %atenuation factor
beta  = imag(gamma);  %real propagation constant

t = linspace(0,1/frec,n_t);
z = linspace(z_min,0,n_z);

Ex_inc = zeros(n_t,n_z);
Ex_refl = zeros(n_t,n_z);
for i = 1:n_t
    Ex_inc(i,:)  = E0*cos(om*t(i) - kz1*z);
    Ex_refl(i,:) = E0*abs(r_p_0)*cos(om*t(i) + kz1*z + angle(r_p_0));
end

z1 = linspace(0,z_max,n_z1);
Ex_tr = zeros(n_t,n_z1);
for i = 1:n_t
    Ex_tr(i,:) = E0*abs(t_p_0)*exp(-alpha*z1).*cos(om*t(i) - beta*z1 + angle(t_p_0));
end

%plot the Reflectance
figure;
set(gcf,'Color','w');
 plot(theta_1*180/pi,abs(r_p),'Color','r','Linewidth',2)
 hold on
 plot(theta_1*180/pi,abs(r_m),'Color','b','Linewidth',2)
 axis([0 90 0.85 1])
 set(gca,'XTick',0:30:90);
 set(gca,'YTick',0.85:0.05:1.0);
 grid on;
 set(gca,'FontSize',14);
 xlabel('Angle of incidence','FontSize',16);
 ylabel('Reflectance','FontSize',16);
 legend('R_{\mid\mid}','R_{\perp}');

%plot the elecric field
f2 = figure;
set(f2,'Color','w');
for  i = 1:n_t
     plot(z,Ex_inc(i,:),'Color','r','Linewidth',2)
     hold on
     plot(z,Ex_refl(i,:),'Color','b','Linewidth',2)
     plot(z,Ex_inc(i,:) + Ex_refl(i,:),'Color','k','Linewidth',2)
     patch('Faces',[1 2 3 4],'Vertices',[0 -2*E0; 0 2*E0; z_max 2*E0; z_max -2*E0],'FaceColor',[0.95 0.95 0.95]);
     plot(z1,Ex_tr(i,:),'Color',[0.8510    0.3255    0.0980],'Linewidth',2)
     hold off;
     axis([z_min  z_max -2*E0 2*E0])
     grid on;
     set(gca,'FontSize',14);
     xlabel('z (nm)','FontSize',16);
     ylabel('Electric field','FontSize',16);
     %lg = legend('E_{x}^{inc}','E_{x}^{refl}','E_{x}','','E_{x}^{tr}');
     %set(lg,'Location','northwest');
     pause(0.1);
     
     drawnow;
        
     %save out the images
     if elment == 1     
        nev = ['C:\Home\Work\Video\p_',int2str(szam),'.bmp'];
        f = getframe(f2);
        [Xpic,map] = frame2im(f);
        imwrite(Xpic,nev,'bmp');
        szam = szam + 1;
     end
end